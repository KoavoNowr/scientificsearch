import React, { Component } from 'react';
import HeaderPage from '../Header/HeaderPage';

export default class NavBar extends Component {
	render() {
		return (
			<div className="Navbar">
				<nav
					className="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white"
					id="sidenav-main"
				>
					<div className="container-fluid">
						{/* <!-- Toggler --> */}
						<button
							className="navbar-toggler"
							type="button"
							data-toggle="collapse"
							data-target="#sidenav-collapse-main"
							aria-controls="sidenav-main"
							aria-expanded="false"
							aria-label="Toggle navigation"
						>
							<span className="navbar-toggler-icon"></span>
						</button>
						{/* <!-- Brand --> */}
						<a className="navbar-brand pt-0" href="#">
							{/* <img src="./assets/img/brand/blue.png" className="navbar-brand-img" alt="..."> */}
						</a>
						{/* <!-- User --> */}
						<ul className="nav align-items-center d-md-none">
							<li className="nav-item dropdown">
								<a
									className="nav-link"
									href="#"
									role="button"
									data-toggle="dropdown"
									aria-haspopup="true"
									aria-expanded="false"
								>
									<div className="media align-items-center">
										<span className="avatar avatar-sm rounded-circle">
											{/* <!-- <img alt="Image placeholder" src="./assets/img/theme/"> --> */}
											Img
										</span>
									</div>
								</a>
								<div className="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
									<div className=" dropdown-header noti-title">
										<h6 className="text-overflow m-0">Username Place</h6>
									</div>
									<a href="#" className="dropdown-item">
										<i className="ni ni-single-02"></i>
										<span>My profile</span>
									</a>
									<a href="#" className="dropdown-item">
										<i className="ni ni-settings-gear-65"></i>
										<span>Settings</span>
									</a>
									{/* <!-- 
              <a href="#" className="dropdown-item">
                <i className="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a> --> */}
									<div className="dropdown-divider"></div>
									<a href="#!" className="dropdown-item">
										<i className="ni ni-user-run"></i>
										<span>Logout</span>
									</a>
								</div>
							</li>
						</ul>
						{/* <!-- Collapse --> */}
						<div className="collapse navbar-collapse" id="sidenav-collapse-main">
							{/* <!-- Collapse header --> */}
							<div className="navbar-collapse-header d-md-none">
								<div className="row">
									<div className="col-6 collapse-brand">
										<a href="#">{/* <img src="./assets/img/brand/blue.png"> */}</a>
									</div>
									<div className="col-6 collapse-close">
										<button
											type="button"
											className="navbar-toggler"
											data-toggle="collapse"
											data-target="#sidenav-collapse-main"
											aria-controls="sidenav-main"
											aria-expanded="false"
											aria-label="Toggle sidenav"
										>
											<span></span>
											<span></span>
										</button>
									</div>
								</div>
							</div>
							{/* <!-- Form --> */}
							<form className="mt-4 mb-3 d-md-none">
								<div className="input-group input-group-rounded input-group-merge">
								<input type="search" className="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search" />
								<div className="input-group-prepend">
									<div className="input-group-text">
									<span className="fa fa-search"></span>
									</div>
								</div>
								</div>
							</form>
							{/* <!-- Navigation --> */}
							<ul className="navbar-nav">
								<li className="nav-item  active ">
									<a className="nav-link  active " href="#">
										<i className="ni ni-tv-2 text-black"></i> Dashboard
									</a>
								</li>
								<li className="nav-item">
									<a className="nav-link " href="#">
										<i className="ni ni-box-2 text-blue"></i> My Searches
									</a>
								</li>
								<li className="nav-item">
									<a className="nav-link " href="#">
										<i className="ni ni-favourite-28 text-red"></i> Favorites
									</a>
								</li>
							</ul>
							{/* <!-- Divider --> */}
							<hr className="my-3" />
							{/* <!-- Heading --> */}
							<h6 className="navbar-heading text-muted">Informations</h6>
							{/* <!-- Navigation --> */}
							<ul className="navbar-nav mb-md-3">
								<li className="nav-item">
									<a className="nav-link" href="#">
										<i className="ni ni-planet"></i> About Us
									</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>

				<div className="main-content">
					{/* <!-- Navbar --> */}
					<nav className="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
						<div className="container-fluid">
							{/* <!-- Brand --> */}
							<a className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="#">
								Dashboard
							</a>
							{/* <!-- Form --> */}
							< form className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
								<div className="form-group mb-0">
								<div className="input-group input-group-alternative">
									<div className="input-group-prepend">
									<span className="input-group-text"><i className="fas fa-search"></i></span>
									</div>
									<input className="form-control" placeholder="Search" type="text" />
								</div>
								</div>
							</form>
							{/* <!-- User --> */}
							<ul className="navbar-nav align-items-center d-none d-md-flex">
								<li className="nav-item dropdown">
									<a
										className="nav-link pr-0"
										href="#"
										role="button"
										data-toggle="dropdown"
										aria-haspopup="true"
										aria-expanded="false"
									>
										<div className="media align-items-center">
											<span className="avatar avatar-sm rounded-circle">
												{/* <!-- <img alt="Image placeholder" src="./assets/img/theme/"> --> */}
												Img
											</span>
											<div className="media-body ml-2 d-none d-lg-block">
												<span className="mb-0 text-sm  font-weight-bold">Username</span>
											</div>
										</div>
									</a>
									<div className="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
										<div className=" dropdown-header noti-title">
											<h6 className="text-overflow m-0">Username Place</h6>
										</div>
										<a href="#" className="dropdown-item">
											<i className="ni ni-single-02"></i>
											<span>My profile</span>
										</a>
										<a href="#" className="dropdown-item">
											<i className="ni ni-settings-gear-65"></i>
											<span>Settings</span>
										</a>
										{/* <!-- 
										<a href="#" className="dropdown-item">
											<i className="ni ni-calendar-grid-58"></i>
											<span>Activity</span>
										</a> --> */}
										<div className="dropdown-divider"></div>
										<a href="#!" className="dropdown-item">
											<i className="ni ni-user-run"></i>
											<span>Logout</span>
										</a>
									</div>
								</li>
							</ul>
						</div>
					</nav>
					{/* <!-- End Navbar --> */}
					
					{/* <!-- Header --> */}
					<div className="header bg-gradient-primary pb-8 pt-5 pt-md-8">
					<br /><br /><br />
						<HeaderPage />
        			</div>
					{/* <!-- End Header --> */}


				</div>
			</div>
		);
	}
}
