import React, { Component } from 'react';
import './Login.css';
import { Card, Input, Button } from 'antd';
import FooterPage from '../Footer/FooterPage';
import axios from 'axios';


export default class Login extends Component {
	constructor(props) {
		super(props);
		this.state = { username: '', password: '', error: false };
	}

	Checklogin() {
		alert('Login');
	}

	Register() {
		alert('Register');
  }
  
  handleChange = (value) => {
    this.setState({username:value.target.value})
  };
  handleChange2 = (value) => {
    this.setState({password:value.target.value})
  };
  
  handleLogin = () => {
    let username = this.state.username;
    let password = this.state.password;
    console.log(this.state.username);
    console.log(this.state.password);

    axios.post("http://localhost:8765/getuser", {
      username: this.state.username,
      password: this.state.password,
    }).then(( res ) => {
      if( res.data.status == 0 ){
        console.log("Error !!!");
        this.setState({
          error: true
        });
      }else{
        console.log("Valide !!!");
        console.log(res.data.response);
        this.props.history.push({
          pathname: '/Home',
          state: {
            userinfos: res.data.response
          }
        });
      }
    }).catch((err=>{
      console.log(err)
    }))
    
	};

	render() {
		console.log('hey ' + this.props.history);
		return (
        <div>
          <Card 
            style={{
              margin: 'auto',
              transform: 'translateY(100%)',
              color: '#64A2EB',
              fontWeight: 'bold',
              borderRadius: 15,
              width: '30%',
              textAlign: "center",
              boxShadow: '1px 1px 15px rgba(0,0,0,0.4)'
            }}
            title={ <b style={{ color: '#64A2EB', fontSize: '20px' }}> LOGIN <b style={{ color: 'grey' }}> | </b> SCIENTIFIC SEARCH </b> }
          >
            
            <div style={{ marginBottom: 16, width: '80%', marginLeft: '10%' }}>
              <Input placeholder="Username" onChange={this.handleChange} />
            </div>
            <div style={{ marginBottom: 16, width: '80%', marginLeft: '10%' }}>
              <Input.Password placeholder="password"  onChange={this.handleChange2} />
            </div>
            {(this.state.error?<div  style={{ color: "red", display: 'inlineblock',marginBottom: 9 }}> Erreur Informations !!! </div>:'')}
            <div style={{ marginBottom: 16, width: '80%', marginLeft: '40%' }}>
              <Button type="primary" icon="login" size='large' onClick={this.handleLogin}>
                Login
              </Button>
            </div>
          </Card>

          <FooterPage />
        </div>
        
    );
	}
}
