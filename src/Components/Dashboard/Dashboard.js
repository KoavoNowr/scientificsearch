import React, { Component } from 'react';
import { Table, Icon, Card } from 'antd';
import './Dashboard.css';

export default class Dashboard extends Component {



	columns = [
		{
			title: 'Title',
			width: '160px',
			sorter: true,
			dataIndex: 'Title',
			render: (d, record) => (
				<div className="table-action">
					<a href={record.uri}>{record.title}</a>
				</div>
			)
		},
		{
			title: 'Author',
			width: '60px',
			sorter: true,
			dataIndex: 'Author'
		},
		{
			title: 'Date',
			width: '60px',
			sorter: true,
			dataIndex: 'Date'
		},
		{
			title: 'Keywords',
			width: '60px',
			sorter: true,
			dataIndex: 'Keywords'
		},
		{
			title: '',
			width: '60px',
			dataIndex: 'SaveToWatchLater',
			render: (d, record) => (
				<div className="table-action">
					<Icon type="plus" />
				</div>
			)
		}
	];


	change=(page,pageSize)=>{
		console.log(page.current,page.defaultPageSize)

	}

	render() {
		var row=[]
		console.log(this.props.data)
		if(this.props.data.length>0){
		let data=this.props.data;
         row=data.map((item,index)=>{
			 let da=new Date(item.loadDate);
            return{
				 key:index,title:item.title,Author:(item.authors?item.authors.map(auth=>{return auth.name}):''),Date:da.toDateString(),Keywords:''
			}
			 
		}) 
		console.log(row)
		
		return (	
				<div className="Scopus">
					<Card
						style={{
							color: '#64A2EB',
							fontWeight: 'bold',
							borderRadius: 15,
							boxShadow: '1px 1px 15px rgba(0,0,0,0.4)'
						}}
						title="Resolts From Scopus"
						extra={<a href="#">{this.props.resultsFound} Resolts</a>}
					>
						<Table
							rowKey="key"
							columns={this.columns}
							dataSource={row}
							pagination={{
								defaultPageSize: 4
							}}
							onChange={this.change}
							size="small"
						/>
					</Card>
				</div>

		);
	}else{
		return <div></div>
	}
	}
}
