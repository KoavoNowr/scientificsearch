import React, { Component } from 'react';
import Login from './Login/Login';
import Home from './Home/Home';
import Dashboard from './Dashboard/Dashboard';
import Accountconfig from './Accountconfig/Accountconfig';
import Favorite from './Favorite/Favorite';
import Searchhistory from './Searchhistory/Searchhistory';
import Register from './Register/Register';
import NavBar from './Navbar/NavBar';
import HeaderPage from './Header/HeaderPage';
import Footer from './Footer/FooterPage';
import { createBrowserHistory } from "history";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

const customHistory = createBrowserHistory();

export default class Routeur extends Component {
	render() {
		return (
			<Router history={ customHistory }>
				<Switch>
					<Route exact path="/" component={ Home } />
					<Route exact path="/login" component={ Login } />
					<Route exact path="/dashboard" component={ Dashboard } />
					<Route exact path="/Accountconfig" component={ Accountconfig } />
					<Route exact path="/Favorite" component={ Favorite } />
					<Route exact path="/Searchhistory" component={ Searchhistory } />
					<Route exact path="/Register" component={ Register } />
					<Route exact path="/Navbar" component={ NavBar } />
					<Route exact path="/HeaderPage" component={HeaderPage} />
					<Route exact path="/Home" component={Home} />
					<Route exect path="/Footer" component={Footer} />
				</Switch>
			</Router>
		);
	}
}
