import React, { Component } from 'react';
import HeaderPage from '../Header/HeaderPage';
import Footer from '../Footer/FooterPage';
// Use prebuilt version of RNVI in dist folder
import { Input, Select, Button, Row, Col, Icon, Checkbox } from 'antd';
import { Layout, Menu, Breadcrumb } from 'antd';
import Dashboard from '../Dashboard/Dashboard';
import './Home.css';
import axios from 'axios';

const { Header, Content } = Layout;
const InputGroup = Input.Group;
const CheckboxGroup = Checkbox.Group;
const plainOptions = [ 'Scopus', 'Web Of Science', 'Google Scholar' ];
const defaultCheckedList = [];

export default class Home extends Component {
	constructor(props) {
		super(props);
		this.wrapperRef = React.createRef();
		this.state = {
			isActived: false,
			scopusRow: [],
			selectWords: '',
			selectSources: ''
		};
	}
	annimated = () => {
		if (!this.state.isActived) {
			const wrapper = this.wrapperRef;
			wrapper.current.classList.toggle('IsSearched');
			this.setState({ isActived: true });
			console.log('salam');
		}
	};
	handleSearch = () => {
		console.log(this.state.checkedList);
		axios
			.post('http://localhost:8765/getSearch', {
				idUser: '1',
				keywords: this.state.selectWords.toString(),
				source: this.state.checkedList,

				offset: 0,
				show: 100
			})
			.then((res) => {
				if (res.data && res.data.resultsFound) {
					console.log(res.data.results);
					this.setState({ scopusRow: res.data });
					this.annimated();
					console.log(this.state.scopusRow);
				} else {
					this.setState({ scopusRow: res.data });
					console.log(this.state.scopusRow.source);
				}
			})
			.catch((err) => {
				console.log(err);
			});
	};

	handleChange = (value) => {
		this.setState({ selectWords: value });
	};

	state = {
		checkedList: defaultCheckedList,
		indeterminate: true,
		checkAll: false
	};

	onChange = (checkedList) => {
		this.setState({
			checkedList,
			indeterminate: !!checkedList.length && checkedList.length < plainOptions.length,
			checkAll: checkedList.length === plainOptions.length
		});
		this.setState({ selectSources: this.state.checkedList });
	};

	onCheckAllChange = (e) => {
		this.setState({
			checkedList: e.target.checked ? plainOptions : [],
			indeterminate: false,
			checkAll: e.target.checked
		});
		this.setState({ selectSources: this.state.checkedList });
	};

	render() {
		if (!this.props.location.state && !this.props.location) {
			this.props.history.push('/Login');
		} else {
			console.log(this.props.location.state.userinfos);
			if (this.state.isActived) {
				console.log(this.state.scopusRow.source.includes('Web Of Science'));
			}
			return (
				<Layout className="layout">
					<HeaderPage />
					<Content style={{ padding: '0 50px' }}>
						<div>
							<div className="searchbare" ref={this.wrapperRef}>
								<Row type="flex" justify="start" className="d-flex justify-content-center">
									<Col style={{ color: '#64A2EB', fontSize: '70px', fontWeight: 'bold' }}>
										{' '}
										SCIENTIFIC SEARCH{' '}
									</Col>
								</Row>

								<Row type="flex" justify="start" className="d-flex justify-content-center">
									<Col style={{ color: '#7B828A', fontSize: '20px' }}>
										{' '}
										Reach allscientific articles in one plateform{' '}
									</Col>
								</Row>

								<Row type="flex" justify="start" className="d-flex justify-content-center">
									<Col> &nbsp; </Col>
								</Row>

								<Row type="flex" justify="start" className="d-flex justify-content-center">
									<Col style={{ width: '80%' }}>
										<InputGroup size="large">
											<Row gutter={2} type="flex" justify="end">
												<Col span={22}>
													<Select
														mode="tags"
														style={{ width: '100%' }}
														placeholder="Add Keywords separated by commas"
														size="large"
														onChange={this.handleChange}
													/>
												</Col>
												<Col span={2}>
													<Button size="large" onClick={this.handleSearch}>
														<Icon type="search" size="large" style={{ height: 25 }} />
													</Button>
												</Col>
											</Row>
										</InputGroup>
									</Col>
								</Row>

								<Row type="flex" justify="start" className="d-flex justify-content-center">
									<Col> &nbsp; </Col>
								</Row>

								<Row type="flex" justify="start" className="d-flex justify-content-center">
									<div style={{ borderBottom: '1px solid #E9E9E9', height: 30 }}>
										<Checkbox
											indeterminate={this.state.indeterminate}
											onChange={this.onCheckAllChange}
											checked={this.state.checkAll}
										>
											Check all <b style={{ color: '#E9E9E9' }}> &nbsp; | &nbsp; </b>
										</Checkbox>
										<CheckboxGroup
											options={plainOptions}
											value={this.state.checkedList}
											onChange={this.onChange}
										/>
									</div>
								</Row>
							</div>
							{this.state.isActived ? (
								<div className="container">
									{this.state.scopusRow.source.includes('Scopus') ? (
										<Dashboard
											data={this.state.scopusRow.results}
											resultsFound={this.state.scopusRow.resultsFound}
										/>
									) : (
										''
									)}
									{this.state.scopusRow.source.includes('Web Of Science') ? (
										<div>In Progress .......</div>
									) : (
										''
									)}
									{this.state.scopusRow.source.includes('Google Scholar') ? (
										<div>In Progress .......</div>
									) : (
										''
									)}
									:
								</div>
							) : (
								''
							)}
						</div>
					</Content>
					{/* <Footer/> */}
				</Layout>
			);
		}
	}
}
