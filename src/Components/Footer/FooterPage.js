import React, { Component } from 'react'
import './FooterPage.css';
import { Layout } from 'antd';

const { Footer } = Layout;

export default class FooterPage extends Component {
  render() {
    return (
      <Footer className="footer">Science Search ©2020 Created by ENSIAS Students</Footer>
    );
  }
}