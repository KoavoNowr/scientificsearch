import React, { Component } from 'react';
import { Drawer, Button } from 'antd';
import { Badge, Dropdown, Avatar, Menu, Icon } from 'antd';
import './HeaderPage.css';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

export default class HeaderPage extends Component {
	state = {
		current: 'mail',
		visible: false
	};
	showDrawer = () => {
		this.setState({
			visible: true
		});
	};
	onClose = () => {
		this.setState({
			visible: false
		});
	};

	render() {
		return (
			<nav className="menuBar">
				<div className="logo">
					<a href="">Scientific Search</a>
				</div>

				{/* User Not Connected */}

				{/* <div className="menuCon">
          <div className="rightMenu">
            <Menu mode="horizontal" className="Menuu">
              <Menu.Item key="mail">
                <a href="" style={{ color: '#7B828A' }}> Sign In </a>
              </Menu.Item>
            </Menu>
          </div>
          <Button className="barsMenu" type="primary" onClick={this.showDrawer}>
            <span className="barsBtn"></span>
          </Button>
          <Drawer
            title="Sience Search"
            placement="right"
            closable={false}
            onClose={this.onClose}
            visible={this.state.visible}
          >             */}
				{/* Right Menu */}
				{/* <Menu mode="horizontal">          
            <Menu.ItemGroup key="mail">
                <a href="" style={{ color: '#7B828A' }}>Sign In</a>
              </Menu.ItemGroup>
            </Menu>
          </Drawer>
        </div> */}

				{/* User Connected */}

				<div className="menuCon">
					<div className="rightMenu">
						<Dropdown
							size="large"
							overlay={
								<Menu>
									<Menu.Item key="display">My Account</Menu.Item>
									<Menu.Item key="Saves">My Saves</Menu.Item>
									<Menu.Item key="History">My history</Menu.Item>
									<Menu.Item key="Notif">
										Notifications <div style={{ color: 'red', display: 'inline' }}> (3) </div>
									</Menu.Item>
									<Menu.Item key="Setting" style={{ borderBottom: '1px solid #7B828A' }}>
										Settings
									</Menu.Item>
									<Menu.Item key="Logout" style={{ color: 'red' }}>
										Log out
									</Menu.Item>
								</Menu>
							}
						>
							<a href="#User">
								<Badge dot>
									<Avatar src="https://www.kindpng.com/picc/m/22-223941_transparent-avatar-png-male-avatar-icon-transparent-png.png" />
									{/* <Avatar shape="square" icon="user" /> */}
								</Badge>
							</a>
						</Dropdown>
					</div>
					<Button className="barsMenu" type="primary" onClick={this.showDrawer}>
						<span className="barsBtn" />
					</Button>
					<Drawer
						title="Sience Search"
						placement="right"
						closable={false}
						onClose={this.onClose}
						visible={this.state.visible}
					>
						{/* Right Menu */}
						<Menu>
							<Menu.Item key="display">My Account</Menu.Item>
							<Menu.Item key="Saves">My Saves</Menu.Item>
							<Menu.Item key="History">My history</Menu.Item>
							<Menu.Item key="Notif">Notifications (3)</Menu.Item>
							<Menu.Item key="Setting" style={{ borderBottom: '1px solid #7B828A' }}>
								Settings
							</Menu.Item>
							<Menu.Item key="Logout" style={{ color: 'red' }}>
								Log out
							</Menu.Item>
						</Menu>
					</Drawer>
				</div>
			</nav>
		);
	}
}
