import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import Routeur from './Components/routeur';
import 'antd/dist/antd.css';

class App extends Component {
	constructor(props) {
		super(props);
		// N’appelez pas `this.setState()` ici !
		this.state = { counter: 2, aa: 3 };
		//this.handleClick = this.handleClick.bind(this);
	}

	render() {
		return <Routeur />;
	}
}

export default App;
